# How 99.9ᵗʰ is relevant ?

The purpose of this page is to show that extremes values must be taken into account for latency measurements.
It can also helps to decide which percentile you should look at for a web page.

The probability for a user to experience a latency at least as high as the nᵗʰ percentile on a page doing x requests is: `1-(n/100)^x`

[https://nin_.gitlab.io/how-999-is-relevant/](https://nin_.gitlab.io/how-999-is-relevant/)

Seen a bug, a typo or a mistake ? You can open an issue here : https://gitlab.com/nin_/how-999-is-relevant/-/issues
